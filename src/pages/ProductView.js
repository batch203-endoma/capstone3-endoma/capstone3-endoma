import { useState, useEffect, useContext } from "react";

import { Container, Card, Button, Row, Col, Form } from "react-bootstrap";
import { useParams, useNavigate, Link } from "react-router-dom";
import Swal from "sweetalert2";
import "../App.css";
import UserContext from "../UserContext";

export default function ProductView() {
  // "useParams" hooks allows us to retrieve the courseId passed via URL
  const { user } = useContext(UserContext);
  const navigate = useNavigate();
  const { productId } = useParams();
  const [productName, setProductName] = useState("");
  const [description, setDescription] = useState("");
  const [image, setImage] = useState("");
  const baseUrl = "../images/";
  const [price, setPrice] = useState(0);
  const [email, setEmail] = useState("");
  const [stock, setStock] = useState("");
  const [productPrice, setProductPrice] = useState(0);
  const [userId, setUserId] = useState("");
  const [productId2, setProductId2] = useState("");
  const [quantity, setQuantity] = useState("");
  const [productName2, setProductName2] = useState("");

  //Checkout SECTION

  //Product section
  useEffect(() => {
    console.log(productId);
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        setProductName(data.productName);
        setDescription(data.description);
        setPrice(data.price);
        setImage(data.image);
        setStock(data.stock);
      });
  }, [productId]);

  const createOrder = (productId) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/order`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        products: [
          {
            userId: userId,
            email: email,
            price: productPrice,
            productName: productName2,
            productId: productId,
            quantity: quantity,
          },
        ],
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data) {
          Swal.fire({
            title: "Check Out successfully",
            icon: "success",
            text: "Thank you for shopping with us",
          });
          navigate("/productCategory");
        } else {
          Swal.fire({
            title: "Something went wrong",
            icon: "error",
            text: "Please try again.",
          });
        }
      });
  };
  return (
    <div className="container-fluid">
      <div className="row mt-5 mx-2">
        <div className="col-md-5 mt-5 col-xs-12">
          <img
            width="400"
            className="card img-fluid"
            src={`../images/${image}.png`}
            alt="Card image cap"
          />
        </div>
        <div className="col-md-5 col-xs-12">
          <h2 className="text-center mb-4">{productName}</h2>
          <h6>Quick Overview: {description}</h6>
          <hr />
          <h1 className="color-green">Php: {price.toLocaleString()}</h1>
          <br />
          <h5>Stock Left: {stock}</h5>
          <Form.Control
            min="1"
            className="mt-5 w-25"
            type="number"
            placeholder="Qty."
            value={quantity}
            onChange={(e) => setQuantity(e.target.value)}
            required
          />
          {user.isAdmin || quantity <= 0 ? (
            <>
              <Button
                onClick={() => createOrder(productId)}
                className="btn-lg btn-success me-5 mt-3"
                disabled
              >
                Buy Now!
              </Button>
              <Button
                as={Link}
                to="/productCategory"
                className="btn-lg btn-secondary mt-3"
              >
                Go back
              </Button>
            </>
          ) : (
            <>
              <Button
                onClick={() => createOrder(productId)}
                className="btn-lg btn-success me-5 mt-3"
              >
                Buy Now!
              </Button>
              <Button
                as={Link}
                to="/productCategory"
                className="btn-lg btn-secondary mt-3"
              >
                Go back
              </Button>
            </>
          )}
        </div>
      </div>
    </div>
  );
}
