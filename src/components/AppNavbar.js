import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import NavDropdown from "react-bootstrap/NavDropdown";
import Offcanvas from "react-bootstrap/Offcanvas";
import { FaShoppingCart } from "react-icons/fa";
import { NavLink, Link, useNavigate, Navigate } from "react-router-dom";
import React, { useState, useEffect, useContext } from "react";
import { Form, Button, Container, Modal, Row, Col } from "react-bootstrap";
import "../App.css";
import Register from "./Register.js";
import Login from "../pages/Login.js";
import Swal from "sweetalert2";
import UserContext from "../UserContext.js";

export default function AppNavbar() {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [showLogin, setShowLogin] = useState(false);
  const handleCloseLogin = () => setShowLogin(false);
  const handleShowLogin = () => setShowLogin(true);

  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [address, setAddress] = useState("");
  const [email, setEmail] = useState("");
  const [passwordLogin, setPasswordLogin] = useState("");
  const [emailLogin, setEmailLogin] = useState("");
  const [mobileNo, setMobileNo] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");
  const [isActive, setIsActive] = useState(false);
  const [password, setPassword] = useState("");
  const { user, setUser } = useContext(UserContext);
  const navigate = useNavigate();

  useEffect(() => {
    if (emailLogin !== "" && passwordLogin !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
    //enable the button if:
    // all the fields are populated
    // both passwords match
  }, [emailLogin, passwordLogin]);

  useEffect(() => {
    if (
      firstName !== "" &&
      lastName !== "" &&
      email !== "" &&
      mobileNo !== "" &&
      address !== "" &&
      password1 !== "" &&
      password2 !== "" &&
      password1 === password2
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [firstName, lastName, email, mobileNo, password1, password2, address]);

  function registerUser(e) {
    // Prevents page loading/ redirection via form submission.
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        email: email,
        address: address,
        password: password1,
        mobileNo: mobileNo,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data == false) {
          Swal.fire({
            title: "Duplicate email found",
            icon: "error",
            text: "Please use other email to complete the registration",
          });
        } else {
          Swal.fire({
            title: "Welcome to DataQuest",
            icon: "success",
            text: "Registration Successful",
          });
          setFirstName("");
          setLastName("");
          setAddress("");
          setEmail("");
          setMobileNo("");
          setPassword1("");
          setPassword2("");
          setShow(false);
        }
      });

    // Notify user for registration
    // alert("Thank you for registering!");
  }

  function loginUser(e) {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: emailLogin,
        password: passwordLogin,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data.accessToken);
        if (data.accessToken !== undefined) {
          localStorage.setItem("token", data.accessToken);
          retrieveUserDetails(data.accessToken);
          Swal.fire({
            title: "Login Successful",
            icon: "success",
            text: "Welcome to DataQuest",
          });
        } else {
          Swal.fire({
            title: "authentication failed",
            icon: "error",
            text: "Something went wrong",
          });
        }
      });
    setEmailLogin("");
    setPasswordLogin("");
    navigate("/");
    setShowLogin(false);
  }

  const retrieveUserDetails = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data._id);
        // Change the global user state to store the "id" and isAdmin
        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
        });
      });
    // Token will be sent as part of the request's header.
  };

  return (
    <>
      <Modal
        show={showLogin}
        onHide={handleCloseLogin}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Body>
          <h1 className="my-5 text-center">Login </h1>
          <Form onSubmit={(e) => loginUser(e)}>
            <Form.Group className="mb-3" controlId="emailLogin">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter Address"
                value={emailLogin}
                required
                onChange={(e) => setEmailLogin(e.target.value)}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="passwordLogin">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                value={passwordLogin}
                onChange={(e) => setPasswordLogin(e.target.value)}
              />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <a href="#" className="me-auto">
            Forgot Password?
          </a>
          <a onClick={handleShow} className="me-auto">
            Sign up
          </a>
          <Button
            variant="secondary"
            type="submit"
            id="submitBtn"
            onClick={handleCloseLogin}
          >
            Cancel
          </Button>
          {isActive ? (
            <Button
              variant="success"
              type="submit"
              id="submitBtn"
              onClick={(e) => loginUser(e)}
            >
              Submit
            </Button>
          ) : (
            <Button variant="success" type="submit" id="submitBtn" disabled>
              Submit
            </Button>
          )}
        </Modal.Footer>
      </Modal>
      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title>Create Account</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Container className="bg-light p-5 mt-3">
            <Form>
              <Row xs={1} md={2}>
                <Form.Group className="mb-3" controlId="firstName">
                  <Form.Label>First Name</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="First Name"
                    value={firstName}
                    onChange={(e) => setFirstName(e.target.value)}
                    required
                  />
                </Form.Group>
                <Form.Group className="mb-3" controlId="lastName">
                  <Form.Label>Last Name</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Last Name"
                    value={lastName}
                    onChange={(e) => setLastName(e.target.value)}
                    required
                  />
                </Form.Group>
              </Row>
              <Form.Group className="mb-3" controlId="email">
                <Form.Label>Email address</Form.Label>
                <Form.Control
                  type="email"
                  placeholder="Enter email"
                  onChange={(e) => setEmail(e.target.value)}
                  value={email}
                  required
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="mobileNo">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Mobile Number"
                  value={mobileNo}
                  onChange={(e) => setMobileNo(e.target.value)}
                  required
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="address">
                <Form.Label>Complete Address</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Address"
                  value={address}
                  onChange={(e) => setAddress(e.target.value)}
                  required
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Password"
                  value={password1}
                  onChange={(e) => setPassword1(e.target.value)}
                  required
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="password2">
                <Form.Label>Confirm Password</Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Verify Password"
                  value={password2}
                  onChange={(e) => setPassword2(e.target.value)}
                  required
                />
              </Form.Group>
            </Form>
          </Container>
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="secondary"
            type="submit"
            id="submitBtn"
            onClick={handleClose}
          >
            Cancel
          </Button>
          {isActive ? (
            <Button
              variant="primary"
              type="submit"
              id="submitBtn"
              onClick={(e) => registerUser(e)}
            >
              Register
            </Button>
          ) : (
            <Button variant="danger" type="submit" id="submitBtn" disabled>
              Register
            </Button>
          )}
        </Modal.Footer>
      </Modal>
      <Navbar bg="dark" className=" box-shadow-md sticky-top">
        <Container fluid className="px-5">
          <Navbar.Brand as={NavLink} to="/">
            <img
              src="https://upload.wikimedia.org/wikipedia/commons/0/09/Blue_computer_icon.svg"
              width="30"
              height="30"
              className="d-inline-block align-top"
              alt="React Bootstrap logo"
            />
          </Navbar.Brand>
          <Nav className="me-auto">
            <Nav.Link as={Link} to="/ProductCategory" className="text-light">
              Products
            </Nav.Link>
            <Nav.Link as={Link} to="/error" className="text-light">
              System Builder
            </Nav.Link>

            {user.id !== null ? (
              user.isAdmin ? (
                <Nav.Link as={NavLink} to="/admin" className="text-light">
                  Admin Dashboard
                </Nav.Link>
              ) : (
                <Nav.Link as={NavLink} to="/Profile" className="text-light">
                  Profile
                </Nav.Link>
              )
            ) : (
              ""
            )}
          </Nav>

          {user.id !== null ? (
            <Nav.Link
              className="text-light ms-auto px-2"
              as={NavLink}
              to="/logout"
              end
            >
              logout
            </Nav.Link>
          ) : (
            <>
              <Nav.Link
                className="text-light ms-auto px-2"
                onClick={handleShowLogin}
              >
                Login
              </Nav.Link>
              <Nav.Link className="text-light px-4" onClick={handleShow}>
                Register
              </Nav.Link>
            </>
          )}
          <Nav.Link as={Link} to="/" className="text-light px-2">
            <FaShoppingCart style={{ fontSize: "30px" }} />
          </Nav.Link>
        </Container>
      </Navbar>
    </>
  );
}
